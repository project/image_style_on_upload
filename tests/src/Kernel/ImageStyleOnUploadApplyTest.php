<?php

namespace Drupal\Tests\image_style_on_upload\Functional;

use Drupal\KernelTests\KernelTestBase;
use Drupal\file\Entity\File;

/**
 * Test the actual image_style_on_upload functionality.
 *
 * @group image_style_on_upload
 */
class ImageStyleOnUploadApplyTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'image',
    'image_style_on_upload',
    'image_style_on_upload_test',
    'system',
    'user',
  ];

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);
    $this->installConfig('image_style_on_upload');
    $this->installConfig('image_style_on_upload_test');

    $this->fileSystem = $this->container->get('file_system');
    $this->imageFactory = $this->container->get('image.factory');
  }

  /**
   * Test that an image style is applied to a saved image.
   */
  public function testImageStyleOnUpload() {
    $this->config('image_style_on_upload.settings')
      ->set('image_style', 'upload_2500')
      ->save();

    $this->fileSystem->copy(__DIR__ . '/../../fixtures/3000x3000.png', 'public://3000x3000-v1.png');
    File::create([
      'uri' => 'public://3000x3000-v1.png',
    ])->save();

    // Check that the image was resized.
    $image = $this->imageFactory->get('public://3000x3000-v1.png');
    $this->assertEquals(2500, $image->getWidth());

    $this->config('image_style_on_upload.settings')
      ->set('image_style', 'upload_1000x1000')
      ->save();

    $this->fileSystem->copy(__DIR__ . '/../../fixtures/3000x3000.png', 'public://3000x3000-v2.png');
    File::create([
      'uri' => 'public://3000x3000-v2.png',
    ])->save();

    // Check that the image was resized.
    $image = $this->imageFactory->get('public://3000x3000-v2.png');
    $this->assertEquals(1000, $image->getWidth());
    $this->assertEquals(1000, $image->getHeight());
  }

}
