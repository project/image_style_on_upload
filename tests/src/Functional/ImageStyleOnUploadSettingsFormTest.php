<?php

namespace Drupal\Tests\image_style_on_upload\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the image_style_on_upload settings form.
 *
 * @group image_style_on_upload
 */
class ImageStyleOnUploadSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['image_style_on_upload_test'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User with 'administer site configuration' permission.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * A regular user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->webUser = $this->createUser();
    $this->adminUser = $this->createUser(['administer site configuration']);
  }

  /**
   * Test access to the settings form.
   */
  public function testSettingsFormAccess() {
    // Anonymous users don't have access to the settings form.
    $this->drupalGet('admin/config/media/image_style_on_upload');
    $this->assertSession()->statusCodeEquals(403);

    // Login as a regular user.
    $this->drupalLogin($this->webUser);
    // This user doesn't have the 'administer site configuration' permission.
    // This user doesn't have access to the settings form.
    $this->drupalGet('admin/config/media/image_style_on_upload');
    $this->assertSession()->statusCodeEquals(403);

    // Login as an admin user.
    $this->drupalLogin($this->adminUser);
    // The admin user has the 'administer site configuration' permission and
    // has access to the settings form.
    $this->drupalGet('admin/config/media/image_style_on_upload');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test the image_style_on_upload settings form.
   */
  public function testSettingsForm() {
    // Check default config.
    $config = $this->config('image_style_on_upload.settings');
    $this->assertEquals('upload', $config->get('image_style'));
    $this->assertEquals('image/gif image/jpeg image/png', $config->get('mime_types'));

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/config/media/image_style_on_upload');
    $this->submitForm([
      'image_style' => 'upload_1000x1000',
      'mime_types' => 'random_mime_type',
    ], 'Save configuration');

    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Check if config is updated.
    $config = $this->config('image_style_on_upload.settings');
    $this->assertEquals('upload_1000x1000', $config->get('image_style'));
    $this->assertEquals('random_mime_type', $config->get('mime_types'));

    // Check the default form values.
    $this->drupalGet('admin/config/media/image_style_on_upload');
    $this->assertSession()->fieldValueEquals('image_style', 'upload_1000x1000');
    $this->assertSession()->fieldValueEquals('mime_types', 'random_mime_type');
  }

}
