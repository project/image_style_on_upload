CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Developers
* Maintainers

INTRODUCTION
------------
Applies an image style when uploading an image file.
This can be handy to reduce the width of images
to a more conventional format.

REQUIREMENTS
------------
- php >= 7.1
- Drupal >= 8.5

INSTALLATION
------------
Install the "Image style on upload" module as
you would normally install a contributed Drupal module.

Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------
By default, an image style and settings are configured.
Users with the "manage site configuration"
permission will be able to select an image style and
select the mime types by visiting: /admin/config/media/image_style_on_upload

DEVELOPERS
----------
* Maarten Heip - https://www.drupal.org/u/mheip

But a special thanks to Intracto, for allowing me to do this on company time.
https://www.intracto.com

MAINTAINERS
-----------
Current maintainers:
* Maarten Heip - https://www.drupal.org/u/mheip
