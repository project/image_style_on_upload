<?php

namespace Drupal\image_style_on_upload\Utility;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\image\ImageStyleInterface;
use Psr\Log\LoggerInterface;

/**
 * Applies the image style on file upload.
 *
 * @package Drupal\image_style_on_upload\Utility
 */
class ImageStyleApplier {

  /**
   * The image style config.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig|null
   */
  protected $imageStyleConfig = NULL;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * ImageStyleApplier constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    ConfigFactory $configFactory,
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    LoggerInterface $logger,
  ) {
    $this->imageStyleConfig = $configFactory->get('image_style_on_upload.settings');
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->logger = $logger;
  }

  /**
   * Applies the file interface.
   *
   * @param \Drupal\file\FileInterface $file
   *   The filter interface.
   *
   * @return bool
   *   TRUE if the apply was successful.
   */
  public function apply(FileInterface $file): bool {
    if (!$this->imageStyleConfig) {
      return TRUE;
    }

    if (!$file->isTemporary()) {
      return TRUE;
    }

    if (!in_array($file->getMimeType(), $this->getConfiguredMimeTypes())) {
      return TRUE;
    }

    $imageStyle = $this->getConfiguredImageStyle();

    if (!$imageStyle instanceof ImageStyleInterface) {
      $this->logger->error('The image style configured for the upload does not exist');
      return FALSE;
    }

    $extension = pathinfo($file->getFileUri(), PATHINFO_EXTENSION);
    $derived_uri = $file->getFileUri() . '.' . $extension;

    if (!$imageStyle->createDerivative($file->getFileUri(), $derived_uri)) {
      $this->logger->error('The uploaded image could not be processed with style %image_style. The image file may be invalid.', ['%image_style' => $imageStyle->getName()]);
      return FALSE;
    }

    if ($this->fileSystem->move($derived_uri, $file->getFileUri(), FileExists::Replace) === FALSE) {
      $this->logger->error('An error occurred while saving the uploaded image file.');
      return FALSE;
    }

    $file->setSize(@filesize($file->getFileUri()));

    return TRUE;
  }

  /**
   * Returns an array of the configured mime types.
   *
   * @return array
   *   An array of the configured mime types.
   */
  protected function getConfiguredMimeTypes(): array {
    $mimeTypes = $this->imageStyleConfig->get('mime_types');

    if (!is_string($mimeTypes) || empty($mimeTypes)) {
      return [];
    }

    return explode(' ', $mimeTypes);
  }

  /**
   * Returns the configured image style.
   *
   * @return \Drupal\image\ImageStyleInterface|null
   *   An image style.
   */
  private function getConfiguredImageStyle(): ?ImageStyleInterface {
    $imageStyle = $this->imageStyleConfig->get('image_style');

    if (empty($imageStyle)) {
      return NULL;
    }

    try {
      $imageStyle = $this->entityTypeManager
        ->getStorage('image_style')
        ->load($imageStyle);
    }
    catch (\Exception $e) {
      $imageStyle = NULL;
    }

    if (!$imageStyle instanceof ImageStyleInterface) {
      return NULL;
    }

    return $imageStyle;
  }

}
