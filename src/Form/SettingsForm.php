<?php

namespace Drupal\image_style_on_upload\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\ImageStyleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The settings form to configure the image style on upload.
 *
 * @package Drupal\image_style_on_upload\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager, used to retrieve the image styles.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $static = parent::create($container);

    $static->setEntityTypeManager(
      $container->get('entity_type.manager')
    );

    return $static;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'image_style_on_upload_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['image_style_on_upload.settings'];
  }

  /**
   * Sets the entity type manager property.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entityTypeManager): void {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('image_style_on_upload.settings');

    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Select an image style that is to be applied on upload of an image file'),
      '#default_value' => $config->get('image_style'),
      '#options' => $this->getImageStyleOptions(),
      '#empty_option' => $this->t('- No image style -'),
    ];

    $form['mime_types'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter the mime types with a space between each one'),
      '#default_value' => $config->get('mime_types'),
      '#maxlength' => 2050,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('image_style_on_upload.settings');

    $config->set(
      'image_style', $form_state->getValue('image_style')
    );

    $config->set(
      'mime_types', $form_state->getValue('mime_types')
    );

    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Returns the image style options.
   *
   * @return array
   *   An array of image style options.
   */
  protected function getImageStyleOptions(): array {
    try {
      $imageStyles = $this->entityTypeManager
        ->getStorage('image_style')
        ->loadMultiple();
    }
    catch (\Exception $e) {
      $imageStyles = [];
    }

    if (empty($imageStyles)) {
      return [];
    }

    $options = [];

    foreach ($imageStyles as $imageStyle) {
      if (!$imageStyle instanceof ImageStyleInterface) {
        continue;
      }

      $options[$imageStyle->id()] = $imageStyle->label();
    }

    asort($options);
    return $options;
  }

}
